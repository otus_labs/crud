package otus.lab.crud;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CRUD_USERS")
@Setter
@Getter
@ToString()
@EqualsAndHashCode
public class User extends AbstractEntity {

    @Column(name = "USERNAME", length = 50, nullable = false, unique=true)
    @NonNull
    private String userName;

    @Column(name = "FIRSTNAME", length = 100, nullable = false)
    @NonNull
    private String firstName;

    @Column(name = "LASTNAME", length = 100, nullable = false)
    @NonNull
    private String lastName;
}